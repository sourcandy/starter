package ginv_test

import (
	"context"
	"fmt"
	"gitee.com/sourcandy/starter/ginv"
	"github.com/gin-gonic/gin"
	"net/http"
	"testing"
)

func TestWrapHandler(t *testing.T) {
	r := gin.Default()
	r.POST("/test1", ginv.WrapHandler(RestMethod))
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"errs": "", "data": "", "msg": "请求成功", "code": "200"})
		return
	})
	r.Run(":8088")
	fmt.Println(111)
}

type TestReq struct {
	Id int32 `json:"id"`
	Name string `json:"name"`
}

type TestRsp struct {
	Ret int32 `json:"ret"`
	Data string `json:"data"`
}


func RestMethod(ctx context.Context,req *TestReq,rsp *TestRsp) error {
	rsp.Ret = 1
	rsp.Data = req.Name
	return fmt.Errorf("%+v","测试")
}