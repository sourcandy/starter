package ginv

import (
	"context"
	"fmt"
	"gitee.com/sourcandy/starter"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"net/http"
	"reflect"
)

var typeContext = reflect.TypeOf(new(context.Context)).Elem()
var typeError = reflect.TypeOf(new(error)).Elem()
var defaultContext = &gin.Context{}

//NewRequestID 获取uuid
func NewRequestID() string {
	return uuid.New().String()
}

func GetGin(ctx context.Context) *gin.Context {
	return defaultContext
}

func GinSet(ctx context.Context,key string, value any)  {
	defaultContext.Set(key , value)
}

func GinGet(ctx context.Context,key string) (value any, exists bool) {
	value,exists = defaultContext.Get(key )
	return
}


func WrapHandler(h interface{}) gin.HandlerFunc {
	if h == nil {
		panic("WrapHandler不能传入空")
	}
	t := reflect.TypeOf(h)
	if t.NumIn() < 3 {
		panic("入参必须填三个参数")
	}
	if reflect.TypeOf(h).Kind() != reflect.Func {
		panic("入参必须为函数")
	}
	if typeContext.AssignableTo(t.In(0)) && t.Out(0).AssignableTo(typeError) {
		return wrapType31(reflect.ValueOf(h))
	}
	panic("未找到此函数")
}

func wrapType31(hv reflect.Value) gin.HandlerFunc {
	t := hv.Type()
	return func(c *gin.Context) {
		var reqValue []reflect.Value
		var rspV reflect.Value
		var req reflect.Value
		var rsp reflect.Value
		ctx := log.With().Str(starter.RequestId,
			NewRequestID()).Logger().WithContext(context.Background())
		reqValue = append(reqValue, reflect.ValueOf(ctx))

		for i := 1; i < t.NumIn(); i++ {
			reqV, _ := inparam(c, t.In(i).Elem())
			reqValue = append(reqValue, reqV)
			if i == 2 {
				rspV = reqV
				rsp = reqV
			}else{
				req = reqV
			}
		}
		defaultContext = c
		out := hv.Call(reqValue)
		es ,_:=out[0].Interface().(error)
		log.Ctx(ctx).Trace().Str("method",c.Request.Method).
			Str("url",c.Request.URL.Path).Str("req",fmt.Sprint(req.Elem())).
			Str("rsp",fmt.Sprint(rsp.Elem())).Err(es).Msg("gin requests")
		DefaultRspRenderer(c,rspV.Interface(),es)
	}
}

func inparam(c *gin.Context, reqs reflect.Type) (reflect.Value, error) {
	var bs []binding.Binding
	b := binding.Default(c.Request.Method, c.ContentType())
	if b == binding.Form {
		bs = []binding.Binding{b, binding.Header}
	} else {
		bs = []binding.Binding{b, binding.Header, binding.Form}
	}
	reqV := reflect.New(reqs)
	req := reqV.Interface()
	for _, b := range bs {
		err := c.ShouldBindWith(req, b)
		if err != nil {
			return reqV, err
		}
	}
	err := c.ShouldBindUri(req)
	if err != nil {
		return reflect.Value{}, err
	}
	return reqV, nil
}


// DefaultRspRenderer 默认的请求结果处理函数
func DefaultRspRenderer(c *gin.Context, rsp interface{}, err error) {
	if err != nil {
		_ = c.Error(err)
		return
	}
	c.JSON(http.StatusOK, rsp)
}