package conf


//BindConf 绑定内容
type BindConf struct {
	BindName string `json:"bind_name"`
	BindContent map[string]interface{} `json:"bind_content"`
}

func (b *BindConf) SetBindName(name string)  {
	b.BindName = name
}

func (b *BindConf) SetBindContent(content map[string]interface{})  {
	b.BindContent = content
}