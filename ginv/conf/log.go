package conf

import (
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"time"
)

// UseLog 日志初始化
func (s *Starter) UseLog(is bool) *Starter {
	if is {
		fmt.Println("log...")
		_dir := s.LogFilepath
		_, err := os.Stat(_dir)
		if err != nil {
			err = os.Mkdir(_dir, os.ModePerm)
			if err != nil {
				log.Err(err).Msg("InitLog err")
				panic(err)
			}
		}
		fileName := time.Now().Format("2006-01-02") + "." + s.LogFileExt
		logFile, err := os.OpenFile(_dir+"/"+fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
		fmt.Println(_dir + "/" + fileName)
		fmt.Println(err)
		consoleWriter := zerolog.ConsoleWriter{Out: os.Stdout}
		multi := zerolog.MultiLevelWriter(consoleWriter, logFile)
		log.Logger = zerolog.New(multi).With().Timestamp().Logger()
		log.Info().Msg("服务启动。。。。")
	}
	return s
}
