package conf

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"strconv"
)

var DefaultContainer = Starter{
	binds: Read(),
}

// Starter 启动容器
type Starter struct {
	binds error `json:"binds"`
	RunMode      string `json:"run_mode"`
	HttpPort     int32  `json:"http_port"`
	ReadTimeout  int32  `json:"read_timeout"`
	WriteTimeout int32  `json:"write_timeout"`
	LogFilepath  string `json:"log_filepath"`
	LogFileExt   string `json:"log_file_ext"`
}

func NewStarter() *Starter {
	if _, ok := ReadConfig["server"]; ok {
		js, _ := json.Marshal(ReadConfig["server"])
		json.Unmarshal(js, &DefaultContainer)
	}
	return &DefaultContainer
}

// NewBindField 绑定mysql，redis，和变量
func (s *Starter) NewBindField(h ...interface{}) *Starter {
	for _, i2 := range h {
		bindConfig(i2)
	}
	return s
}

func (s *Starter) Run(opt func(e *gin.Engine)) error {
	var err error
	es := gin.Default()
	gin.SetMode(s.RunMode)
	opt(es)
	err = es.Run(":" + strconv.Itoa(int(s.HttpPort)))
	return err
}
