package conf

import (
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

type Config struct {
	Mysql MysqlConfig `mapstructure:"mysql" yaml:"mysql"`
	Redis RedisConfig `mapstructure:"redis" json:"redis"`
}

type Reader interface {
	Read()
}
var ReadConfig map[string]interface{}

func Read(path ...string) error {
	var config string
	if len(path) == 0 {
		//flag.StringVar(&config, "c", "", "config file")
		//flag.Parse()
		if config == "" {
			config = "../config.yaml"
			//fmt.Printf("使用默认值，config的路径为%v\n", config)
		} else {
			//fmt.Printf("使用-c传递的值，config的路径为%v\n", config)
		}
	} else {
		config = path[0]
		//fmt.Printf("使用传递单值，config的路径为%v\n", config)
	}
	v := viper.New()
	v.SetConfigName("config")
	v.AddConfigPath("../config")
	v.AddConfigPath(".")
	v.AddConfigPath("../")
	v.AddConfigPath("./config")
	//v.SetConfigFile(config)
	v.SetConfigType("yaml")
	err := v.ReadInConfig()
	if err != nil {
		panic(err)
	}
	v.WatchConfig()
	v.OnConfigChange(func(in fsnotify.Event) {
		ReadConfig = v.AllSettings()
	})
	ReadConfig = v.AllSettings()
	return nil
}