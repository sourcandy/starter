package conf_test

import (
	"fmt"
	"gitee.com/sourcandy/starter/ginv/conf"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
	"net/http"
	"testing"
)

//AMSConfi ...
type AMSConfig struct {
	AppID      string
	ActivityID string
}
type TgclubConfig struct {
	AppID      string
	ActivityID string
}
var ValueConfig = struct {
	XinyueAMSConf      *AMSConfig
	TgclubAMSConf      *TgclubConfig
}{}

var ValueConfig2 = struct {
	XinyueAMSConf      *AMSConfig
	TgclubAMSConf      *TgclubConfig
}{}

var DbConfig = struct {
	DbMysql                *gorm.DB
}{}

type List struct {
	Id int `json:"id"`
}

func TsestRouter(r *gin.Engine) {
	r.GET("/ping", func(c *gin.Context) {
		fmt.Println("ValueConfig的值：", ValueConfig.XinyueAMSConf)
		list := List{}
		err := DbConfig.DbMysql.Table("user").Where("id = ? ",10).Find(&list).Error
		fmt.Println("DbConfig的值：",err)
		fmt.Println(list)
		log.Info().Msg("测试啊")
		c.JSON(http.StatusOK, gin.H{"errs": "" ,"data": "", "msg": "请求成功", "code":"200"})
		return
	})
}

//,&DbConfig
func TestNewContainer(t *testing.T) {
	conf.NewStarter().NewBindField(&ValueConfig,&DbConfig).UseLog(true).Run(TsestRouter)

}

