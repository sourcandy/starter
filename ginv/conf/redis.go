package conf

import "time"


//RedisConfig redis配置
type RedisConfig struct {
	Type        string `mapstructure:"type" yaml:"type"`
	Host        string `mapstructure:"host" yaml:"host"`
	Port        int32 `mapstructure:"port" yaml:"port"`
	Password    string `mapstructure:"password" yaml:"password"`
	MaxIdle     int `mapstructure:"max-idle" yaml:"max-idle"`
	MaxActive   int `mapstructure:"max-active" yaml:"max-active"`
	IdleTimeout time.Duration `mapstructure:"idle-timeout" yaml:"idle-timeout"`
}