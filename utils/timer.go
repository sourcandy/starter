package utils

import (
	"gitee.com/sourcandy/starter/errs"
	"time"
)

// NowStartEnd 当天的开始和结束时间
func NowStartEnd() (time.Time, time.Time) {
	now := time.Now()
	star := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local)
	ends := time.Date(now.Year(), now.Month(), now.Day(), 23, 59, 59, 0, time.Local)
	return star, ends
}

// WeekStartEnd 当周的开始和结束时间
func WeekStartEnd() (time.Time, time.Time) {
	now := time.Now()
	star := time.Date(now.Year(), now.Month(), now.Day()-int(now.Weekday())+1, 0, 0, 0, 0, time.Local)
	ends := time.Date(now.Year(), now.Month(), now.Day()+6-int(now.Weekday())+1, 23, 59, 59, 0, time.Local)
	return star, ends
}

// MonthStartEnd 当月的开始和结束时间
func MonthStartEnd() (time.Time, time.Time) {
	now := time.Now()
	firstOfMonth := time.Date(now.Year(), now.Month(), 1, 0, 0, 0, 0, now.Location())
	tem := firstOfMonth.AddDate(0, 1, -1)
	return firstOfMonth, time.Date(tem.Year(), tem.Month(), tem.Day(),
		23, 59, 59, 0, now.Location())
}

// GetDateList 获取时间段内的日期
func GetDateList(start, end time.Time, format string) ([]string, error) {
	if format == "" {
		format = "2006-01-02"
	}
	list := []string{}
	if start.Unix() > end.Unix() {
		return list, errs.New(-1, "开始时间不能大于解释时间！")
	}
	for t := start; !t.After(end); t = t.AddDate(0, 0, 1) {
		list = append(list, t.Format(format))
	}
	return list, nil
}

// GetStringToTime 字符串转换为time
func GetStringToTime(times, format string) (time.Time, error) {
	if format == "" {
		return time.Time{}, errs.New(-3, "格式不能为空！")
	}
	return time.ParseInLocation(format, times, time.Local)
}

// GetIntToTime 时间戳转time
func GetIntToTime(cuo int64) time.Time {
	return time.Unix(cuo, 0)
}
