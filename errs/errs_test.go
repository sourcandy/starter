package errs_test

import (
	"fmt"
	"gitee.com/sourcandy/starter/errs"
	"testing"
)

func TestErrTur_Error(t *testing.T) {
	var err error
	err = errs.New(1002,"测试呀")
	fmt.Println(err)
	err = errs.New(102,"dss").WithDetail("测试%s","错误的日志")
	fmt.Println(err)
}