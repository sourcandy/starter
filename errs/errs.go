package errs

import (
	"fmt"
	"github.com/pkg/errors"
)

type ErrTur struct {
	Code   Code
	Msg    string
	Detail string
}

// Error error的接口实现
func (e *ErrTur) Error() string {
	return fmt.Sprintf("error code:%d, msg:%s, detail:%s", e.Code, e.Msg, e.Detail)
}

func (e *ErrTur) GetCode() int32 {
	return int32(e.Code)
}
func (e *ErrTur) GetMsg() string {
	return e.Msg
}

func (e *ErrTur) GetDetail() string {
	return e.Detail
}

// WithDetail 额外的参数显示
func (e *ErrTur) WithDetail(format string, a ...any) error {
	return fmt.Errorf("code:%d,msg:%s;"+format, e.Code, e.Msg, a)
}

// Wrap 额外的参数显示
func (e *ErrTur) Wrap(err error) error {
	return fmt.Errorf("code:%d,msg:%s;err:%v", e.Code, e.Msg, err)
}

type Code int32

func (c Code) Error(msg string) *ErrTur {
	return &ErrTur{
		Code: c,
		Msg:  msg,
	}
}

func (c Code) ErrorWithDetail(msg, detail string) *ErrTur {
	return &ErrTur{
		Code:   c,
		Msg:    msg,
		Detail: detail,
	}
}

func (c Code) GetDetail(err error) string {
	err = errors.Cause(err)
	if err == nil {
		return ""
	}
	if e, ok := err.(*ErrTur); ok {
		return e.Detail
	} else {
		return ""
	}
}

func (c Code) GetCode(err error) Code {
	err = errors.Cause(err)
	if err == nil {
		return 0
	}
	if e, ok := err.(*ErrTur); ok {
		return e.Code
	} else {
		return -1
	}
}

func (c Code) GetMsg(err error) string {
	err = errors.Cause(err)
	if err == nil {
		return ""
	}

	if e, ok := err.(*ErrTur); ok {
		return e.Msg
	} else {
		return err.Error()
	}
}
