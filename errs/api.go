package errs

// New ...
func New(code int, msg string) *ErrTur {
	return Code(code).Error(msg)
}

// NewDetail ...
func NewDetail(code int, msg string, detail string) *ErrTur {
	return Code(code).ErrorWithDetail(msg, detail)
}
