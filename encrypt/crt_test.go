package encrypt_test

import (
	"fmt"
	"gitee.com/sourcandy/starter/encrypt"
	"testing"
)

func TestEncryptCrt(t *testing.T){
	source := "hello world"
	fmt.Println("原字符：", source)
	key := "1443flfsaWfdasds"
	encryptCode, _ := encrypt.AesCtrCrypt([]byte(source), []byte(key))
	fmt.Println("密文：", string(encryptCode))
	decryptCode, _ := encrypt.AesCtrCrypt(encryptCode, []byte(key))
	fmt.Println("解密：", string(decryptCode))
}
