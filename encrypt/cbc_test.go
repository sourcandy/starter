package encrypt_test

import (
	"fmt"
	"gitee.com/sourcandy/starter/encrypt"
	"testing"
)

func TestEncryptCba(t *testing.T){
	orig := "hello world"
	key := "0123456789012345"
	fmt.Println("原文：", orig)
	encryptCode := encrypt.AesEncrypt(orig, key)
	fmt.Println("密文：", encryptCode)
	decryptCode := encrypt.AesDecrypt(encryptCode, key)
	fmt.Println("解密结果：", decryptCode)
}


