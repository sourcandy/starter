package encrypt_test

import (
	"encoding/hex"
	"fmt"
	"gitee.com/sourcandy/starter/encrypt"
	"testing"
)

func TestEncryptCFB(t *testing.T){
	source := "imslimsl"
	fmt.Println("原字符：", source)
	source1 := "e07454996f04fbdccfc2acddbf1a7507bc61a4373e2cd039"
	fmt.Println("加密字符：", source)
	key := "DrvlKOAbcxbxqMKb" //16位
	encryptCode := encrypt.AesEncryptCFB([]byte(source), []byte(key))
	fmt.Println("密文：", hex.EncodeToString(encryptCode))
	str, _ := hex.DecodeString(source1)
	decryptCode := encrypt.AesDecryptCFB(str, []byte(key))
	fmt.Println("解密：", string(decryptCode))
}
