package encrypt_test

import (
	"encoding/hex"
	"fmt"
	"gitee.com/sourcandy/starter/encrypt"
	"testing"
)

func TestEncryptObf(t *testing.T){
	source := "hello world"
	fmt.Println("原字符：", source)
	key := "1111111111111111" //16位  32位均可
	encryptCode, _ := encrypt.AesEncryptOFB([]byte(source), []byte(key))
	fmt.Println("密文：", hex.EncodeToString(encryptCode))
	decryptCode, _ := encrypt.AesDecryptOFB(encryptCode, []byte(key))
	fmt.Println("解密：", string(decryptCode))
}