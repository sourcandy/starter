package encrypt

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
)

//Md5String md5加密字段
func Md5String(pwd string) string {
	data := []byte(pwd)
	md5New := md5.New()
	md5New.Write(data)
	// hex转字符串
	md5String := hex.EncodeToString(md5New.Sum(nil))
	return md5String
}


//Md5ByteString md5加密字段
func Md5ByteString(pwd []byte) string {
	res := md5.Sum(pwd)
	return fmt.Sprintf("%x", res)
}

