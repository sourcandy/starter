import xlrd
import json
import sys

if __name__ == '__main__':
    paths = sys.argv
    list = {}
    for s in range(len(paths)):
        if s > 0:
            workbook = xlrd.open_workbook(paths[s])
            worksheet = workbook.sheet_by_index(0)
            nrows = worksheet.nrows  # 获取该表总行数
            ncols = worksheet.ncols  # 获取该表总列数
            arr = []
            for i in range(nrows):
                cell = []
                for j in range(ncols):
                    cell.append(worksheet.cell_value(i, j))
                arr.append(cell)
            list[paths[s]] = arr
    print(json.dumps(arr))
