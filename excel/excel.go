package excel

import (
	"encoding/json"
	"os/exec"
)

func Read(path ...string)  {
	list := map[string][][]string{}
	args := []string{"./script/reader.py"}
	for _, s := range path {
		args = append(args,s)
	}
	cmd,_ := exec.Command("python",args...).Output()
	json.Unmarshal(cmd,&list)
}